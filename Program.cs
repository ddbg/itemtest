﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using KellermanSoftware.CompareNetObjects;
using Newtonsoft.Json;

namespace itemtest
{
    /// <summary>
    /// Default class.
    /// </summary>
    class Program
    {
        private static string _strDefaultHostName = "us.battle.net";
        private static string _strDefaultKnownGoodPath = Directory.GetCurrentDirectory() + "\\known_goods\\";

        /// <summary>
        /// The entry point of the program.
        /// </summary>
        /// <param name="args">The arguments.</param>
        private static void Main(string[] args)
        {
            Console.WriteLine("World of Warcraft Item API tester\n" +
                              "Created by: Nemanja Mulasmajic\n");

            Console.Write("[+] Enter host [{0}]: ", _strDefaultHostName);
         
            // This program can optionally take 2 parameters. 
            //      - The first is the host of the web API.
            //      - The second is the path to the folder that contains the known-goods database.
            
            string strHostName;
            if (args.Length >= 1) /* Do we have an argument passed in for the host string? */
            {
                strHostName = args[0]; // If so, let's use it.
                Console.WriteLine(strHostName);
            }
            else
                strHostName = Console.ReadLine(); // Otherwise, we should request it from the user.

            // If there no host was specified, use the default one.
            if (!string.IsNullOrEmpty(strHostName))
                _strDefaultHostName = strHostName;

            Console.Write("[+] Enter \"known-goods\" location [{0}]: ", _strDefaultKnownGoodPath);
            
            string strKnownGoodPath;
            if (args.Length >= 2) /* Do we have an argument passed in for the known-goods path? */
            {
                strKnownGoodPath = args[1]; // If so, let's use it.
                Console.WriteLine(strKnownGoodPath);
            }
            else
                strKnownGoodPath = Console.ReadLine(); // Otherwise, we should request it from the user.
                
            // If there is no known-goods path specified, use the default one.
            if (!string.IsNullOrEmpty(strKnownGoodPath))
                _strDefaultKnownGoodPath = strKnownGoodPath;

            IEnumerable<string> lstItemPaths;
            IEnumerable<string> lstItemSetPaths;
            try
            {
                // Load up the paths to all the "item_*" and "itemset_*" files in the known-goods database. 
                lstItemPaths = Directory.EnumerateFiles(_strDefaultKnownGoodPath, "item_*");
                lstItemSetPaths = Directory.EnumerateFiles(_strDefaultKnownGoodPath, "itemset_*");
            }
            catch (DirectoryNotFoundException)
            {
                Console.WriteLine("[-] ERROR: Could not find folder \"{0}\". Please make sure it exists.", _strDefaultKnownGoodPath);
                goto error;
            }
            catch (UnauthorizedAccessException)
            {
                Console.WriteLine("[-] ERROR: Could not open folder \"{0}\". Do you have the necessary permissions?", _strDefaultKnownGoodPath);
                goto error;
            }
            catch (Exception exception)
            {
                Console.WriteLine("[-] ERROR: Unhandled exception: {0}.", exception);
                goto error;
            }

            // Serialize the items in the known-goods database. Create .NET objects out of them.
            List<Item> lstItems = new List<Item>();
            List<ItemSet> lstItemSets = new List<ItemSet>();
            try
            {
                foreach (string itemPath in lstItemPaths)
                {
                    Item item = JsonConvert.DeserializeObject<Item>(File.ReadAllText(itemPath));
                    lstItems.Add(item);
                }

                foreach (string itemsetPath in lstItemSetPaths)
                {
                    ItemSet itemset = JsonConvert.DeserializeObject<ItemSet>(File.ReadAllText(itemsetPath));
                    lstItemSets.Add(itemset);
                }
            }
            catch (JsonException exception)
            {
                // If an object from the known-goods database failed to convert into something that is .NET friendly (valid JSON),
                // execution should halt.

                // There should be no errors in the known-good database. It's supposed to be good!
                Console.WriteLine("[-] ERROR: Could not serialize file. Is it in valid JSON format? {0}", exception);
                goto error;
            }
            catch (AccessViolationException exception)
            {
                Console.WriteLine("[-] ERROR: Failed to open file. Do you have the necessary permissions? {0}", exception);
                goto error;
            } 
            catch (Exception exception)
            {
                Console.WriteLine("[-] ERROR: Unhandled exception: {0}.", exception);
                goto error;
            }

            // Print out some information.
            Console.WriteLine("[+] Loaded {0} items and {1} item sets from known-goods database.", lstItems.Count, lstItemSets.Count);
            
            // Start the testing!
            Console.WriteLine("\n{0}\n\tBEGIN TESTS\n{1}", new string('-', 30), new string('-', 30));
            Tests test = new Tests(_strDefaultHostName, lstItems, lstItemSets);
            int nElapsedTime = test.Run();

            // Output some statistics.
            Console.WriteLine("\n[+] Tests completed in {0} milliseconds.", nElapsedTime);
            
        error:
            if (args.Length == 0)
            {
                Console.WriteLine("Press [ENTER] to continue.");
                Console.ReadLine();
            }
        }

    }

    /// <summary>
    /// The class that will test the features of the World of Warcraft item web API.
    /// </summary>
    internal class Tests
    {
        private readonly List<Item> _lstItems;
        private readonly List<ItemSet> _lstItemSets;
        private readonly string _strHostName;
        private readonly WebClient _webClient;
        private readonly CompareLogic _compareLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="Tests"/> class.
        /// </summary>
        /// <param name="strHostName">Name of the host that interfaces with the API.</param>
        /// <param name="lstItems">The collection of items.</param>
        /// <param name="lstItemSets">The collection of item sets.</param>
        public Tests(string strHostName, List<Item> lstItems, List<ItemSet> lstItemSets)
        {
            _strHostName = strHostName;
            _lstItems = lstItems;
            _lstItemSets = lstItemSets;
            _webClient = new WebClient();
            _compareLogic = new CompareLogic();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="Tests"/> class.
        /// </summary>
        ~Tests()
        {
            _webClient.Dispose();
        }

        /// <summary>
        /// Runs every test case against the web API.
        /// </summary>
        public int Run()
        {
            int nOldTickCount = Environment.TickCount;

            try
            {
                bool bPreliminaryTestsSucceeded = Test1();
                Console.WriteLine("");

                bPreliminaryTestsSucceeded = Test2() && bPreliminaryTestsSucceeded;
                Console.WriteLine("");

                bPreliminaryTestsSucceeded = Test3() && bPreliminaryTestsSucceeded;
                Console.WriteLine("");

                // Tests 1-3 are considered "core". More advanced features of the item API should not be tested unless
                // the "core" tests succeed.
                if (!bPreliminaryTestsSucceeded)
                    Console.WriteLine("[-] ERROR: More extensive tests cannot be run until the prior tests are successful.");
                else
                {
                    Test4();
                    Console.WriteLine("");

                    Test5();
                    Console.WriteLine("");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine("[-] ERROR: Failed to run tests, {0}.", exception);
            }

            return (Environment.TickCount - nOldTickCount);
        }

        /// <summary>
        /// Test #1: 
        ///     Ensures that every item in the known-goods database is equivalent (e.g. all properties match)
        ///     to that returned by the web API.
        /// </summary>
        /// <returns>Returns true if there are no inconsistencies.</returns>
        private bool Test1()
        {
            Console.WriteLine("[+] Test 1 :: Checking expected values of items in the known-goods database against actual response from item web API...");

            int nTotalFailed = 0;

            // Loop through every item in the known-goods database.
            foreach (Item item in _lstItems)
            {
                Console.WriteLine("\tQuerying web API for item ID {0}.", item.id);

                string strResponse;
                try
                {
                    strResponse = _webClient.DownloadString(string.Format("http://{0}/api/wow/item/{1}", _strHostName, item.id));
                }
                catch (WebException exception)
                {
                    HttpWebResponse webResponse = (HttpWebResponse)exception.Response;
                    if (webResponse.StatusCode == HttpStatusCode.NotFound)
                    {
                        Console.WriteLine("\t\t- WARNING: Item {0} was not found.", item.id);
                        nTotalFailed++;
                        continue;
                    }
                    
                    throw;
                }

                Console.WriteLine("\t\t- Retrieved response from server.");
                
                Item webItem = JsonConvert.DeserializeObject<Item>(strResponse);
                Console.WriteLine("\t\t- Deserialized JSON object.");
                
                ComparisonResult comparisonResult = _compareLogic.Compare(item, webItem);
                Console.WriteLine("\t\t- Compared known-good item to item retrieved from web API.");

                if (!comparisonResult.AreEqual)
                {
                    Console.WriteLine("\t\t- WARNING: Item {0} does not contain expected values.", item.id);
                    Console.WriteLine(comparisonResult.DifferencesString);
                    nTotalFailed++;
                }
                else
                {
                    Console.WriteLine("\t\t- SUCCESS: Item {0}'s actual value matches what is expected.", item.id);
                }
            }

            Console.WriteLine("[+] Test 1 :: Complete. There were {0} out of {1} items that failed the test.", nTotalFailed, _lstItems.Count);
            return (nTotalFailed == 0);
        }

        /// <summary>
        /// Test #2: 
        ///     Ensures that every item set in the known-goods database is equivalent (e.g. all properties match)
        ///     to that returned by the web API.
        /// </summary>
        /// <returns>Returns true if there are no inconsistencies.</returns>
        private bool Test2()
        {
            Console.WriteLine("[+] Test 2 :: Checking expected values of item sets in the known-goods database against actual response from item web API...");

            int nTotalFailed = 0;

            foreach (ItemSet itemSet in _lstItemSets)
            {
                Console.WriteLine("\tQuerying web API for item set ID {0}.", itemSet.id);

                string response;
                try
                {
                    response = _webClient.DownloadString(string.Format("http://{0}/api/wow/item/set/{1}", _strHostName, itemSet.id));
                }
                catch (WebException exception)
                {
                    HttpWebResponse webResponse = (HttpWebResponse)exception.Response;
                    if (webResponse.StatusCode == HttpStatusCode.NotFound)
                    {
                        Console.WriteLine("\t\t- WARNING: Item set {0} was not found.", itemSet.id);
                        nTotalFailed++;
                        continue;
                    }

                    throw;
                }

                Console.WriteLine("\t\t- Retrieved response from server.");

                ItemSet webItemSet = JsonConvert.DeserializeObject<ItemSet>(response);
                Console.WriteLine("\t\t- Deserialized JSON object.");

                ComparisonResult comparisonResult = _compareLogic.Compare(itemSet, webItemSet);
                Console.WriteLine("\t\t- Compared known-good item set to item set retrieved from web API.");

                if (!comparisonResult.AreEqual)
                {
                    Console.WriteLine("\t\t- WARNING: Item set {0} does not contain expected values.", itemSet.id);
                    Console.WriteLine(comparisonResult.DifferencesString);
                    nTotalFailed++;
                }
                else
                {
                    Console.WriteLine("\t\t- SUCCESS: Item set {0}'s actual value matches what is expected.", itemSet.id);
                }
            }

            Console.WriteLine("[+] Test 2 :: Complete. There were {0} out of {1} item sets that failed the test.", nTotalFailed, _lstItemSets.Count);
            return (nTotalFailed == 0);
        }

        /// <summary>
        /// Test #3: 
        ///     Ensures that invalid item and item sets ID yield the expected 404 "Not Found" response.
        /// </summary>
        /// <returns>Returns true if there are no inconsistencies.</returns>
        private bool Test3()
        {
            Console.WriteLine("[+] Test 3 :: Checking for invalid item and item sets...");

            Queue<int> queueBadValues = new Queue<int>(5);
            queueBadValues.Enqueue(Int32.MinValue);
            queueBadValues.Enqueue(-1);
            queueBadValues.Enqueue(0);
            queueBadValues.Enqueue(1337);
            queueBadValues.Enqueue(Int32.MaxValue);

            int nTotalFailed = 0;
            string strBaseURL = string.Format("http://{0}/api/item", _strHostName);
            
            while (queueBadValues.Count > 0)
            {
                int nID = queueBadValues.Dequeue();

                string strBadItemURL = string.Format("{0}/{1}", strBaseURL, nID);
                string strBadItemSetURL = string.Format("{0}/set/{1}", strBaseURL, nID);

                for (int i = 0; i < 2; i++)
                {
                    bool bError = false;

                    Console.WriteLine("\tQuerying web API for bad {0} ID {1}", ((i == 0) ? "item" : "item set"), nID);

                    try
                    {
                        _webClient.DownloadString(((i == 0) ? strBadItemURL : strBadItemSetURL));
                    }
                    catch (WebException exception)
                    {
                        bError = true;

                        HttpWebResponse webResponse = (HttpWebResponse) exception.Response;
                        if (webResponse.StatusCode == HttpStatusCode.NotFound)
                            Console.WriteLine("\t\t- SUCCESS: Retrieved (404) \"Not Found\" response from server as expected.");
                        else
                        {
                            Console.WriteLine("\t\t- WARNING: Retrieved unexpected error code for web response, {0}.", webResponse.StatusDescription);
                            nTotalFailed++;
                        }
                    }

                    if (!bError)
                    {
                        Console.WriteLine("\t\t- WARNING: Retrieved sucessful response from server. This should not happen...");
                        nTotalFailed++;
                    }
                }
            }

            Console.WriteLine("[+] Test 3 :: Complete. There were {0} failed test cases.", nTotalFailed);
            return (nTotalFailed == 0);
        }

        /// <summary>
        /// Test #4:
        ///     Ensures that for every item in the known-goods database that belongs to an item set, 
        ///     the web API is queried on the item set to confirm that the item is present there as well.
        /// </summary>
        /// <returns>Returns true if there are no inconsistencies.</returns>
        private bool Test4()
        {
            Console.WriteLine("[+] Test 4 :: Checking reference of every item that belongs to an item set in the known-goods database...");

            int nTotalFailed = 0;
            int nTotalItemsWithSets = 0;

            foreach (Item item in _lstItems)
            {
                if (item.itemSet == null)
                    continue;

                nTotalItemsWithSets++;

                Console.WriteLine("\tItem ID {0} has item set {1} associated with it.", item.id, item.itemSet.id);
                Console.WriteLine("\t\t- Querying web API for item set ID {0}.", item.itemSet.id);

                string strResponse;
                try
                {
                    strResponse = _webClient.DownloadString(string.Format("http://{0}/api/wow/item/set/{1}", _strHostName, item.itemSet.id));
                }
                catch (WebException exception)
                {
                    HttpWebResponse webResponse = (HttpWebResponse)exception.Response;
                    if (webResponse.StatusCode == HttpStatusCode.NotFound)
                    {
                        Console.WriteLine("\t\t- WARNING: Item set {0} was not found.", item.itemSet.id);
                        nTotalFailed++;
                        continue;
                    }

                    throw;
                }

                Console.WriteLine("\t\t- Retrieved response from server.");

                ItemSet webItemSet = JsonConvert.DeserializeObject<ItemSet>(strResponse);
                Console.WriteLine("\t\t- Deserialized JSON object.");

                if (!webItemSet.items.Contains(item.id))
                {
                    Console.WriteLine("\t\t- WARNING: Item set {0} does not contain item {1} as expected.", webItemSet.id, item.id);
                    nTotalFailed++;
                }
                else
                {
                    Console.WriteLine("\t\t- SUCCESS: Item set {0} contains item {1}.", webItemSet.id, item.id);   
                }
            }

            Console.WriteLine("[+] Test 4 :: Complete. There were {0} out of {1} items that failed the test.", nTotalFailed, nTotalItemsWithSets);
            return (nTotalFailed == 0);
        }

        /// <summary>
        /// Test #5:
        ///     Ensures that for every item set in the known-goods database, the web API is queried for each
        ///     item of that item set to confirm that the item is part of the item set. 
        /// </summary>
        /// <returns>Returns true if there are no inconsistencies.</returns>
        private bool Test5()
        {
            Console.WriteLine("[+] Test 5 :: Checking reference of all items in the item sets of known-goods database...");

            int nTotalFailed = 0;
            int nTotalComparisons = 0;

            foreach (ItemSet itemSet in _lstItemSets)
            {
                Console.WriteLine("\tRetrieving all items for item set ID {0}.", itemSet.id);

                foreach (int nID in itemSet.items)
                {
                    nTotalComparisons++;

                    Console.WriteLine("\t\t- Querying web API for item {0}.", nID);

                    string strResponse;
                    try
                    {
                        strResponse = _webClient.DownloadString(string.Format("http://{0}/api/wow/item/{1}", _strHostName, nID));
                    }
                    catch (WebException exception)
                    {
                        HttpWebResponse webResponse = (HttpWebResponse)exception.Response;
                        if (webResponse.StatusCode == HttpStatusCode.NotFound)
                        {
                            Console.WriteLine("\t\t- WARNING: Item {0} was not found.", nID);
                            nTotalFailed++;
                            continue;
                        }

                        throw;
                    }

                    Console.WriteLine("\t\t- Retrieved response from server.");

                    Item webItem = JsonConvert.DeserializeObject<Item>(strResponse);
                    Console.WriteLine("\t\t- Deserialized JSON object.");

                    if (webItem.itemSet != null && webItem.itemSet.id == itemSet.id && webItem.itemSet.items.Contains(nID))
                    {
                        Console.WriteLine("\t\t- SUCCESS: Item {0} is part of item set {1}.", webItem.id, itemSet.id);
                    }
                    else
                    {
                        Console.WriteLine("\t\t- WARNING: Item {0} is not part of item set {1} as expected.", webItem.id, itemSet.id);
                        nTotalFailed++;
                    }
 
                }
            }

            Console.WriteLine("[+] Test 5 :: Complete. There were {0} out of {1} items that failed the test.", nTotalFailed, nTotalComparisons);
            return (nTotalFailed == 0);
        }
    }
}
